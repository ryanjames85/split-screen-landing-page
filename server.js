const connect = require('connect');
const serveStatic = require('serve-static');
const open = require('open');

connect().use(serveStatic(__dirname)).listen(8080, function(){
    // Opens the url in the default browser
    open('http://localhost:8080/index.html');
    console.log('Server running on 8080...');
    console.log('Open in a Browser');
    console.log('http://localhost:8080/index.html');
});