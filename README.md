# Split Screen landing Page
>
> Simple split landing example.

## Building the app
>
>run
>***npm install***
> install the modules needed to run the local server.

## Running the Application
>
>run
>***npm run start***
>to start a local server

## Running in Dev mode
>
>run
>***npm run dev***
>starts the server and watches for file changes and reloads the server